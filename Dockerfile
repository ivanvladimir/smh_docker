FROM python:3.6-stretch

MAINTAINER Ivan Vladimir Meza Ruiz "ivanvladimir@gmail.com"

RUN apt-get update \
    && apt-get -y install cmake swig3.0 libpython3-all-dev

RUN apt-get -y install cmake build-essential pkg-config libgoogle-perftools-dev

RUN mkdir -p /process
RUN mkdir -p /data
WORKDIR /process

RUN git clone https://github.com/gibranfp/Sampled-MinHashing.git
RUN cd Sampled-MinHashing \
    && git branch -a \
    && git checkout python3
RUN cd Sampled-MinHashing \
    && mkdir build \
    && cd build \
    && cmake ..\
    && make \
    && make install

ADD requirements.txt /process
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

ADD . /process
