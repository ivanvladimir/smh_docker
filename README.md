# Sampled Min-Hashing Docker

Docker for Sample Ming Hashing with some scripts to process the files


# Steps

1. Create a link from directory with files to ./data

2. Run docker bash

    docker-compose run process bash

3. Create corpus file and vocabulary

    python dir2corpus.py transform /data

4. Mine topics

    python discover.py process corpus.ids

5. Translate topics to words

    python discover.py explore corpus.model voca.txt


Topics will be available at corpus.topics
