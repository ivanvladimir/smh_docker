#!/usr/bin/python
import click
import os.path
import re
import numpy as np
from colorama import Fore, Back, Style
from sklearn.feature_extraction.text import CountVectorizer

@click.group()
def cli():
    pass

@cli.command()
@click.argument('dirname', type=click.Path(exists=True))
@click.option('filter_re','--filter', type=str)
def list(dirname,filter_re=None):
    if filter_re:
        f_reg=re.compile(filter_re)

    for r,d,fs in os.walk(dirname):
        for f in fs:
            filename=os.path.join(r, f)
            if not filter_re or f_reg.search(filename):
                print(filename)


@cli.command()
@click.argument('dirname', type=click.Path(exists=True))
@click.option('filter_re','--filter', type=str)
@click.option('odir','--odir', type=str,default="./")
@click.option('voca_file','--vocafile', type=str,default="voca.txt")
@click.option('stop_words','--stop_words', type=str,default=None)
@click.option('corpus_file','--corpusfile', type=str,default="corpus.ids")
@click.option('min_df','--min', type=int, default=10)
@click.option('max_df','--max', type=float, default=0.9)
def transform(dirname,filter_re,stop_words,min_df,max_df,corpus_file,voca_file,odir):
    if filter_re:
        f_reg=re.compile(filter_re)

    newline_reg=re.compile(r'\n+')
    digits_reg=re.compile(r'\d+')
    guion_reg=re.compile(r'_+')


    stopwords=[]
    if stop_words:
        for line in open(stop_words):
            line=line.strip()
            stopwords.append(line)
    cv=CountVectorizer(lowercase=True,min_df=min_df,max_df=max_df,stop_words=stopwords)

    docs=[]
    for r,d,fs in os.walk(dirname):
        for f in fs:
            filename=os.path.join(r, f)
            if not filter_re or f_reg.search(filename):
                with open(filename) as ff:
                    s1=newline_reg.sub(" ",ff.read())
                    s2=digits_reg.sub(" ",s1)
                    s3=guion_reg.sub(" ",s2)
                    docs.append(s3)

    X=cv.fit_transform(docs)
    X_total=np.array(np.sum(X,axis=0))

    voca_filename=os.path.join(odir,voca_file)
    print(Fore.YELLOW, f"Creating vocabulary file {voca_filename}", Fore.RESET)
    with open(voca_filename,'w') as ff:
        for i,word in enumerate(cv.get_feature_names()):
            freq=X_total[0][i]
            print(f"{i} {word} {freq}",file=ff)

    corpus_filename=os.path.join(odir,corpus_file)
    print(Fore.BLUE, f"Creating corpus file {corpus_filename}", Fore.RESET)
    with open(corpus_filename,'w') as ff:
        cx = X.tocoo()
        idoc=-1
        doc=[]
        for d,w,freq in zip(cx.row, cx.col, cx.data):
            if not idoc == d:
                if doc:
                    line=" ".join(doc)
                    print(f"{len(doc)} {line}",file=ff)
                idoc=d
                doc=[]
            doc.append(f"{w}:{freq}")



if __name__ == '__main__':
    cli()
