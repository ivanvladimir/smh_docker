#!/usr/bin/python
import click
import os.path
import re
from colorama import Fore, Back, Style
from timeit import default_timer as timer
from datetime import timedelta
from collections import Counter

@click.group()
def cli():
    pass

@cli.command()
@click.argument('corpus_file',type=str)
@click.argument('ids_file',type=str)
@click.option('r','-r', type=int, default=3)
@click.option('l','-l', type=int, default=255)
@click.option('o','-o', type=float, default=0.7)
@click.option('c','-c', type=int, default=5)
def process(corpus_file,ids_file,r,l,o,c):
    start=timer()
    ifs=os.path.splitext(corpus_file)[0]+".ifs"
    ids=os.path.splitext(corpus_file)[0]+".ids"
    cmd=f"smhcmd ifindex {ids} {ifs}"
    print(Fore.RED,"Executing... ",Fore.YELLOW,cmd,Fore.RESET)
    os.system(cmd)
    end=timer()
    print(Fore.GREEN,"Elapsed time",str(timedelta(seconds=end-start)),Fore.RESET)
    start=timer()
    model=os.path.splitext(corpus_file)[0]+".model"
    cmd=f"smhcmd discover -r {r} -l {l} -o {o} -c {c} {ifs} {model}"
    print(Fore.RED,"Executing... ",Fore.YELLOW,cmd,Fore.RESET)
    os.system(cmd)
    end=timer()
    print(Fore.GREEN,"Elapsed time",str(timedelta(seconds=end-start)),Fore.RESET)

@cli.command()
@click.argument('model_file',type=str)
@click.argument('voca_file',type=str)
@click.option('n','-n', type=int, default=None)
def explore(model_file,voca_file,n):
    print(Fore.GREEN,"Reading vocavulary file",voca_file,Fore.RESET)
    i2v={}
    with open(voca_file) as vv:
        for line in vv:
            line=line.strip()
            bits=line.split()
            i2v[bits[0]]=bits[1]


    print(Fore.GREEN,"Reading model file",model_file,Fore.RESET)
    topics=os.path.splitext(model_file)[0]+".topics"
    topics_file=open(topics,"w")
    print(Fore.GREEN,"Creating topics",topics,Fore.RESET)
    with open(model_file) as mm:
        for line in mm:
            line=line.strip()
            bits=line.split()[1:]
            topic=Counter()
            for bit in bits:
                bit=bit.split(":")
                topic[i2v[bit[0]]]=int(bit[1])
            topic=[f"{k}:{v}" for k,v in topic.most_common(n)]
            print(" ".join(topic),file=topics_file)
    topics_file.close()

if __name__ == '__main__':
    cli()
